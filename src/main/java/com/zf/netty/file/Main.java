package com.zf.netty.file;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.net.InetAddress;
import java.net.ServerSocket;

import org.apache.log4j.Logger;

public class Main {
	private static final Logger LOGGER = Logger.getLogger(Main.class);
	public static String appUrl = "https://gitee.com/javactony/fast-scanqrcode-download";
	public static String appTitle = "快捷扫码下载by.Tony";
	public static String appStartDesc = "点击查看源码&Star";
	public static String fileName = "";
	public static void main(final String[] args) {
		// 参数
		if (args.length < 1 || args[0] == null || args[0].equals("")) {
			LOGGER.warn("参数错误");
			System.exit(1);
		}
		fileName = args[0];

		try {
			// 打开一个可视化窗口用来展示二维码
			new SwingPane().go();
		} catch (Exception e) {
			LOGGER.error(e);
			System.exit(1);
		}
	}
}
